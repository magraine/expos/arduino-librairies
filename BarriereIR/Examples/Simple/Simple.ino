#include <BarriereIR.h>
/**
 * Nécessite une diode émettrice IR et une diode réceptrice IR à la même fréquence.
 *
 * Émetteur :
 * 
 *   (D.E)
 *   |   | > Masse
 *   |
 *   < [ résistance, exemple 10kOhm ] < pin émettrice PWM
 *  
 *   Petite résistance = grande distance d'émission;
 *   Grande résistance = courte distance d'émission;
 *
 * Récepteur :
 *   (D.R)     1 < [ résistance 100 Ohm ] < +5V
 *   | | |     2 > Masse
 *   3 2 1     3 > pin réceptrice analogique
 *   
 *   Possibilité de mettre entre 1 et 2 un condensateur >= 4,7 uF
**/


/**
 * Premier paramètre : 
 *    Pin digital PWM
 *    C'est la broche qui va à la diode émettrice.
 * Second paramètre : 
 *    Pin analogique
 *    Broche venant de la diode réceptrice
 */
BarriereIR IR = BarriereIR(2, A0);

// diode de control : s'allume lorsque le faisceau est coupé.
int LedPin = 13;

void setup() {
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, LOW); 
}

void loop() {
  if (IR.tester()) {
    digitalWrite(LedPin, HIGH);
  } else {
    digitalWrite(LedPin, LOW); 
  }
  
  delay(100);
}
