
/**
 * BarriereIR.h
 *
 * - Librairie pour creer barrières infrarouge
 * 
 * @version 2.1.0
 * @author Matthieu Marcillaud <marcimat@magraine.net>
 * @license GNU/GPL
 */


#ifndef BarriereIR_h
#define BarriereIR_h

#include "Arduino.h"



/*
 * La classe prend 2 parametres en entree :
 * - La broche émetrice, qui doit être une PDW
 * - La broche réceptrice qui doit être analogique.
 *
 * Optionnellement,
 * - la frequence de pulsation attendue du recepteur
 * 
 * ?? non codé ?
 * - une broche à alimenter uniquement au moment de la reception pour alimenter le recepteur.
 * 
 */
class BarriereIR
{
  private:
    void Constructeur(int pin_emetteur_ir, int pin_recepteur_ir, unsigned int frequence, int pin_recepteur_ir_alim);

 
  public:
    BarriereIR(int pin_emetteur_ir, int pin_recepteur_ir);
    BarriereIR(int pin_emetteur_ir, int pin_recepteur_ir, unsigned int frequence);
    BarriereIR(int pin_emetteur_ir, int pin_recepteur_ir, unsigned int frequence, int pin_recepteur_ir_alim);

    int IR_PIN_OUT;
    int IR_PIN_IN;
    unsigned int IR_FREQUENCE;

    bool tester();
    void setFrequence(unsigned int frequence);
    void demarrerEmetteur();
    void demarrerEmetteur(int duree);
    void stopperEmetteur();
    int lireRecepteur();
    bool lireRecepteurBool();
};

#endif

