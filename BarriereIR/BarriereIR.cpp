//      BarriereIR.cpp
//      
//      Copyright 2011 Matthieu Marcillaud <marcimat@magraine.net>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.
//      

#include "Arduino.h"
#include "BarriereIR.h"


// constructeurs
BarriereIR::BarriereIR (int pin_emetteur_ir, int pin_recepteur_ir, unsigned int frequence) {
	Constructeur(pin_emetteur_ir, pin_recepteur_ir, frequence, 0);
}

BarriereIR::BarriereIR (int pin_emetteur_ir, int pin_recepteur_ir) {
	Constructeur(pin_emetteur_ir, pin_recepteur_ir, 0, 0);
}

void BarriereIR::Constructeur (int pin_emetteur_ir, int pin_recepteur_ir, unsigned int frequence, int pin_recepteur_ir_alim) {
	// definir la pin emettrice
	IR_PIN_OUT = pin_emetteur_ir;
	pinMode(IR_PIN_OUT, OUTPUT);

	// definir la pin receptrice (analog)
	IR_PIN_IN = pin_recepteur_ir;
	
	// definir la frequence
	if (!frequence) {
		frequence=38000;
	}
	setFrequence(frequence);
}

/**
 * Fonction principale pour tester le contact IR entre l'emetteur
 * et le recepteur.
 *
 * @return bool
 *   - true Si le contact est fermé (pas de réception du signal, barrière coupée)
 *   - false Sinon (réception du signal, barrière libre)
 **/
bool BarriereIR::tester() {
	demarrerEmetteur();
	// minimum = 6/f = 6/38000 = 263 microsecondes
	delayMicroseconds(300);
	bool value = lireRecepteurBool();
	stopperEmetteur();
	return value;
}

void BarriereIR::setFrequence(unsigned int frequence){
	IR_FREQUENCE = frequence;
}

void BarriereIR::demarrerEmetteur(){
	// Attention : tone() ne peut en gérer qu'un à la fois !
	// arduino.cc/en/Reference/Tone
	tone(IR_PIN_OUT, IR_FREQUENCE);
}

void BarriereIR::demarrerEmetteur(int duree){
	// Attention : tone() ne peut en gérer qu'un à la fois !
	// arduino.cc/en/Reference/Tone
	tone(IR_PIN_OUT, IR_FREQUENCE, duree);
}

void BarriereIR::stopperEmetteur(){
	noTone(IR_PIN_OUT);
}

int BarriereIR::lireRecepteur() {
	return analogRead(IR_PIN_IN);
}

bool BarriereIR::lireRecepteurBool() {
	// 3 mesures pour être moins sensibles à de petits aléas
	int val =
		  analogRead(IR_PIN_IN)
		+ analogRead(IR_PIN_IN)
		+ analogRead(IR_PIN_IN);
	if ((val / 3) > 500) {
		return 1;
	}
	return 0;
}



