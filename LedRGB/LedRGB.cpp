#include "Arduino.h"
#include "LedRGB.h"


// constructeur
LedRGB::LedRGB (int redPin, int greenPin, int bluePin) {
  _redPin   = redPin;
  _greenPin = greenPin;
  _bluePin  = bluePin;
  pinMode(_redPin, OUTPUT); 	// sets the ledPin to be an output
  pinMode(_greenPin, OUTPUT); 	// sets the redPin to be an output
  pinMode(_bluePin, OUTPUT); 	// sets the greenPin to be an output
}

// attribuer la couleur
void LedRGB::color(unsigned char red, unsigned char green, unsigned char blue) {
  analogWrite(_redPin, red); 	 
  analogWrite(_bluePin, blue);
  analogWrite(_greenPin, green);
}

// attribuer la couleur
void LedRGB::color(Color couleur) {
  color(couleur.red, couleur.green, couleur.blue);
}

// aller rapidement vers la couleur
void LedRGB::toColor (unsigned char red, unsigned char green, unsigned char blue) {
  toColor(red, green, blue, 10); 
}

// aller rapidement vers la couleur
void LedRGB::toColor (Color couleur) {
  toColor(couleur.red, couleur.green, couleur.blue); 
}

// aller en un temps donne vers une couleur
void LedRGB::toColor (Color couleur, unsigned char interval) {
  toColor(couleur.red, couleur.green, couleur.blue, interval); 
}

// aller en un temps donne vers une couleur
void LedRGB::toColor (unsigned char red, unsigned char green, unsigned char blue, unsigned char interval)
{
    int pasR = 1;
    if (_lastR > red) { pasR = -1; }
    
    int pasG = 1;
    if (_lastG > green) { pasG = -1; }
    
    int pasB = 1;
    if (_lastB > blue) { pasB = -1; }
    
    int okB = 1;
    int okG = 1;
    int okR = 1;
   
    do {
      
      okB = okG = okR = 1;
      
      if (_lastB != blue) { 
        _lastB += pasB;
        okB = 0;
      }
      
      if (_lastG != green) {
        _lastG += pasG;
        okG = 0;
      }
      
      if (_lastR != red) {
        _lastR += pasR; 
        okR = 0;
      }
      color(_lastR, _lastG, _lastB);
      
      // delai entre 2 mouvements de couleur
      delay(interval);
      
    } while ( !(okB && okG && okR) );
}

// clignote une fois
void LedRGB::blink (Color color_1, Color color_2) {
	blink(color_1, color_2, 100);
}

// clignote une fois, avec tempo
void LedRGB::blink (Color color_1, Color color_2, unsigned char interval) {
	color(color_1);
	delay(interval);
	color(color_2);
	delay(interval);
	color(0, 0, 0);
}

// clignote x fois, avec tempo
void LedRGB::blink (Color color_1, Color color_2, unsigned char interval, unsigned char times) {
	for (int x = 0; x<times; x++) {
		color(color_1);
		delay(interval);
		color(color_2);
		delay(interval);
	}
	toColor(0, 0, 0);
}
