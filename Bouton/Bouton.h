//      Bouton.h
//      
//      Copyright 2011 Matthieu Marcillaud <marcimat@magraine.net>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.
//


/*
 *
 * Bouton.h - Librairie pour creer barrières infrarouge
 * version 1.0.0
 * 
 */


#ifndef Bouton_h
#define Bouton_h

#include "Arduino.h"

#define BOUTON_SAFE_DELAY 30
#define BOUTON_MULTICLICK_DELAY 500
#define BOUTON_HOLD_DELAY 1000

/*
 * 
 */
class Bouton
{
  private:
    void Constructeur();
    bool state, lastState;
    unsigned long firstOn, firstOff;
    unsigned long lastOn, lastOff;
    unsigned long delaySafeOn, delaySafeOff; // ms
    unsigned long delayHoldOn, delayHoldOff; // ms
    unsigned long delayClick; // ms
    unsigned long lastClick; // ms
    bool changedOn, changedOff;
    bool changedSafeOn, changedSafeOff;
    bool flagChangedOn, flagChangedOff, flagClick;
    int click;
    
  public:
    Bouton();
    void addValue(bool value);
    void reset();
	bool isOn();
	bool isOff();
	bool isSafeOn();
	bool isSafeOff();
	bool isHoldOn();
	bool isHoldOff();
	bool hasChanged();
	bool hasChangedOn();
	bool hasChangedOff();
	bool hasChangedSafeOn();
	bool hasChangedSafeOff();
	void addClick();
	int  nbClick();
};

#endif

