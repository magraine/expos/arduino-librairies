#include "Arduino.h"
#include "Moyenne.h"


// constructeur
Moyenne::Moyenne (int nombre_elements) {
  const int nb = nombre_elements;
  int elements[nb];
  total = current = 0;
  // initialisation
  for (int i=0; i<nb; i++) {
	elements[i] = 0;
  }
}

int Moyenne::ajouter(int valeur) {
  total -= elements[current];
  elements[current] = valeur;
  total += valeur;
  if (++current >= nb) current=0;
  return (int) (total/nb);
}
