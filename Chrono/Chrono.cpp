#include "Arduino.h"
#include "Chrono.h"


// constructeur
Chrono::Chrono (void) {
	_start_time = 0;
	_pause_time = 0;
	_is_paused  = false;
}

// initialiser le chrono
void Chrono::start() {
	_start_time = millis();
	_pause_time = 0;
	_is_paused = false;
}

// arrêter le chrono
void Chrono::stop() {
	_start_time = 0;
	_pause_time = 0;
}

// le chrono est-il arrêté, prêt à servir ?
bool Chrono::ready() {
	return (_start_time == 0) && (_pause_time == 0);
}

// redémarrer le chrono
void Chrono::reset() {
	stop();
	start();
}

// pause du chrono (ou le remet en route)
void Chrono::pause() {
	if (!_is_paused) {
		_is_paused = true;
		_pause_time += (millis() - _start_time);
	} else {
		_is_paused = false;
		_start_time = millis();
	}
}

// retourne la valeur du chrono
unsigned long Chrono::get() {
	if (_is_paused) {
		return _pause_time;
	} else {
		if (_pause_time || _start_time) {
			return _pause_time + (millis() - _start_time);
		}
		return 0;
	}
}

// retourne true si le temps transmis dépasse la valeur du chrono
bool Chrono::after(unsigned long time) {
	if (ready()) {
		return false;
	}
	return (get() >= time);
}
