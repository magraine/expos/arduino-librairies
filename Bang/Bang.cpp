#include "Arduino.h"
#include "Bang.h"


/**
 * Constructeur
 */
Bang::Bang (void) {
	_bang = false;
	_has_changed = false;
}


/**
 * Mettre à jour le bang
 * 
 * @param bool bang Nouvelle valeur du bang
 * @return bool
 *     - true si c'est une valeur différente de l'ancienne
 *     - false sinon
 */
bool Bang::update(bool bang) {
	if (bang != _bang) {
		_bang = bang;
		_has_changed = true;
	} else {
		_has_changed = false;
	}
	return _has_changed;
}


/**
 * Mettre à jour le bang, et ne retourne un bang que si sa valeur est la même que celle transmise
 * 
 * @param bool bang Nouvelle valeur du bang
 * @param bool etat État à tester
 * @return bool
 *     - true si c'est une valeur différente de l'ancienne et de même état que celui à tester
 *     - false sinon
 */
bool Bang::update(bool bang, bool etat) {
	return update(bang) && (bang == etat);
}

/**
 * Retourne l'état bang de la dernière mise à jour
 * 
 * @return bool
 *     - true si la dernière mise à jour avait une valeur différente de la précédente
 *     - false sinon
 */
bool Bang::get() {
	return _has_changed;
}

/**
 * Compare l'état bang de la dernière mise à jour au bang et état transmis
 *
 * @param bool etat
 *     État à tester
 * @return bool
 *     - true si la dernière mise à jour avait une valeur différente de la précédente
 *       et de même état que celui testé
 *     - false sinon
 */
bool Bang::get(bool etat) {
	return _has_changed && (_bang == etat);
}
