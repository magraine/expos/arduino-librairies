
/**
 * Bang.h
 *
 * Librairie pour envoyer 1 seule impulsion lors d'un changement d'état
 * 
**/

#ifndef Bang_h
#define Bang_h

#include "Arduino.h"


class Bang
{
  public:
    Bang(void);
    bool update(bool bang);
    bool update(bool bang, bool etat);
    bool get();
    bool get(bool etat);
  private:
    bool _bang;
    bool _has_changed;
};

#endif

